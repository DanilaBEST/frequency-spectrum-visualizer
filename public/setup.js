//get display mode
let process = window.require('electron').remote.process;
let storagePath = (process.env.APPDATA || (process.platform == 'darwin' ? process.env.HOME + '/Library/Preferences' : process.env.HOME + "/.local/share")) + '/fsv';
let remote = window.require('electron').remote;
let fs = remote.require('fs');
let display3D = false;

try {
    let json = fs.readFileSync(storagePath + "/config.json", 'utf8') || "{}";
    let config = JSON.parse(json);
    display3D = config.threeD;
} catch (e) {
    console.log("Config File missing");
    console.log("Creating Config File");
    fs.writeFileSync(storagePath + "/config.json", "{}");
}

let mic = undefined;
let fft = undefined;
let aCtx;
let analyser;
let FFTData;
let microphone;

let precision = 8192;


function setup() {
    if (display3D) {
        createCanvas(1300, 1152, WEBGL);
        setAttributes('antialias', true);

        noFill();
        box(50);
    } else {
        createCanvas(1300, 1152);
        setAttributes('antialias', true);
    }

    noFill();

    frameRate(60);

    FFTData = new Uint8Array(precision);
    //setup mic
    navigator.getUserMedia({audio: true}, function (stream) {
        aCtx = new (window.AudioContext || window.webkitAudioContext)();
        analyser = aCtx.createAnalyser();
        analyser.smoothingTimeConstant = 0;
        analyser.fftSize = precision * 2;
        microphone = aCtx.createMediaStreamSource(stream);
        microphone.connect(analyser);
    }, error => {
        throw error;
    });
}

draw = () => {
};
