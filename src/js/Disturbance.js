class Disturbance {
    constructor() {
        this._line_last = 0;
        this._line_backwards = 0;
    }

    line(arr) {

        if (this._line_backwards) {
            for (let i = 0; i < arr.length; i++) {
                let tmp_val = 255 - (Math.abs(i - this._line_last) * 64);
                arr[i] = tmp_val > 0 ? tmp_val : 0;
            }
        } else {
            for (let i = 0; i < arr.length; i++) {
                let tmp_val = 255 - (Math.abs(1024 - i - this._line_last) * 64);
                arr[i] = tmp_val > 0 ? tmp_val : 0;
            }
        }


        if (++this._line_last === 1024) {
            this._line_last = 0;
            this._line_backwards = 1 - this._line_backwards;
        }
    }

    whiteNoise(arr) {
        for (let i = 0; i < arr.length; i++) {
            arr[i] = Math.floor(Math.random() * 255)
        }
    }
}

module.exports = new Disturbance();