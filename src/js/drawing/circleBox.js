let Filter = require('../filter/index');
let conf = require("../Config")


class circleBox {
    constructor() {
        this._rotation = 0;
    }

    draw(lastZoomed, lastFiltered, lastFilteredColored) {

        //todo use mean from history

        let all = 0;
        for (let i = 0; i < 1024; i += 4) {
            all += lastFiltered[i];
        }
        let mean = all / 256;
        let rgba = [0, 0, 0, 64];
        //use the color of the mean value
        Filter.colors[conf.colorMode](mean, rgba)


        stroke(0);
        fill([0,0,0,64])
        square(1300 - 256, 512, 256, 256);


        noFill();
        for (let i = 0; i < 8; i++) {
            stroke(128-i*10);
            fill([20-i*2,20-i*2,20-i*2,32])
            circle(1300 - 128, 512 + 128, 248 - i * 15);
        }

        this._rotation -= 0.01


        let originX = 1300 - 128;
        let originY = 512 + 128;
        let ih = 0;
        let jh = 0;
        let step = PI / 512;



        beginShape();
        fill(rgba)
        stroke(255);


        for (let i = 0; i < 1024; i += 2) {

            ih = Math.sin(this._rotation + i * step);
            jh = Math.cos(this._rotation + i * step);

            vertex(
                (lastFiltered[i] >> 2) * ih + (ih * 64) + originX,
                (lastFiltered[i] >> 2) * jh + (jh * 64) + originY
            )
        }
        endShape(CLOSE);


        fill(mean/8)
        circle(originX, originY, 128)
    }
}

module.exports = new circleBox();