let distanceMatrix = [];

for (let i = 0; i < 1300; i++) {
    let row = [];

    for (let j = 0; j < 900; j++) {
        let val = Math.hypot(i - 650, (j - 450)) / 790 * 1023;
        let truncVal = Math.trunc(val);

        row.push([truncVal, val]);
    }

    distanceMatrix.push(row);
}

let display = new p5.Image(1300, 1252);


let eventManager = require("./events/eventManger");

let updateRules = {
    upDownSmall: require('./updateRules/upDownSmall'),
    circular: require('./updateRules/circular'),
}

module.exports = (spectrum, spectrumCol) => {
    eventManager.applyFilter(distanceMatrix);

    let sum = 0;
    for (let i = 0; i < 1024; i++) {
        sum += spectrum[i];
    }
    let mean = sum / 1024;

    display.loadPixels();

    updateRules.circular(display, spectrumCol, distanceMatrix, mean);

    display.updatePixels();

    image(display, 0, 0);
};