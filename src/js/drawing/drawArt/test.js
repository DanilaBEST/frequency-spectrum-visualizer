let makeNoise2D = require("open-simplex-noise").makeNoise2D;
let noise = makeNoise2D(3);

let last = [];
for (let i = 0; i < 1024; i++) {
    last.push(0.5);
}


let cycleMid = 0;

module.exports = (spectrum, spectrumCol) => {
    background(0);


    for (let i = 0; i < 1024; i++) {
        stroke(spectrumCol[i]);
        circle(650, 450, i * 1.55)
    }

    let sum = 0;
    for (let i = 0; i < 1024; i++) {
        sum += spectrum[i]
    }

    let mean = sum / 1024 / 512 + 0.5;

    for (let i = 0; i < mean/8; i++) {
        last.push(mean);
    }


    for (let i = 0; i < 1024; i++) {
        stroke([
            spectrumCol[i][0] * last[last.length - i - 1],
            spectrumCol[i][1] * last[last.length - i - 1],
            spectrumCol[i][2] * last[last.length - i - 1],
        ]);

        let eyeFactor = 32 / i;
        eyeFactor = eyeFactor > 0.7 ? 0.7 : eyeFactor;
        let offsetX = noise(cycleMid, 0) * eyeFactor * 20;
        let offsetY = noise(0, cycleMid) * eyeFactor * 20;


        ellipse(650 + offsetX, 450 + offsetY, i * 1.55, i * (1.55 - eyeFactor))
    }


    cycleMid += 0.0125;
    cycleMid += 0.025 * (mean - 0.5);
}


