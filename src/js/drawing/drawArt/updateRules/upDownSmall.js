let point = 0;

module.exports = (display, spectrum, distanceMatrix) => {
    for (let j = point; j < 900; j += 8) {

        for (let i = 0; i < 1300; i++) {

            let index = (i + j * 1300) * 4;
            let val = spectrum[distanceMatrix[i][j][0]];

            display.pixels[index + 0] = val[0];
            display.pixels[index + 1] = val[1];
            display.pixels[index + 2] = val[2];
            display.pixels[index + 3] = 255;

        }
    }


    point++;
    point = point > 8 ? point = 0 : point;
}