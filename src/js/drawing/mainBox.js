class mainBox {
    constructor() {
        this.image = undefined;


    }

    init() {
        this.image = new p5.Image(1024, 512 + 256)

        this.image.loadPixels();
        for (let x = 0; x < this.image.width; x++) {

            for (let i = 0; i < 256 + 512; i++) {


                let index = (x + i * this.image.width) * 4;

                this.image.pixels[index] = 0;
                this.image.pixels[index + 1] = 0;
                this.image.pixels[index + 2] = 0;
                this.image.pixels[index + 3] = 255;
            }
        }
        this.image.updatePixels();
    }

    draw(lastZoomed, lastFiltered, lastFilledColored) {

        if (!this.image) {
            this.init();
        }

        this.image.loadPixels();

        //set pixel
        for (let x = 0; x < this.image.width; x++) {
            for (let i = 0; i < 260; i++) {
                let index = (x + i * this.image.width) * 4;

                this.image.pixels[index] = lastFilledColored[x][0];
                this.image.pixels[index + 1] = lastFilledColored[x][1];
                this.image.pixels[index + 2] = lastFilledColored[x][2];
                this.image.pixels[index + 3] = 255;
            }
        }


        //draw black and white to display seconds
        if (new Date().getTime() % 2000 < 1000) {
            let index = (260 * this.image.width) * 4;

            this.image.pixels[index] = 255;
            this.image.pixels[index + 1] = 255;
            this.image.pixels[index + 2] = 255;
            this.image.pixels[index + 4] = 255;
            this.image.pixels[index + 5] = 255;
            this.image.pixels[index + 6] = 255;
        } else {
            let index = (260 * this.image.width) * 4;

            this.image.pixels[index] = 0;
            this.image.pixels[index + 1] = 0;
            this.image.pixels[index + 2] = 0;
            this.image.pixels[index + 4] = 0;
            this.image.pixels[index + 5] = 0;
            this.image.pixels[index + 6] = 0;
        }


        //move pixel
        for (let i = 512 + 256; i >= 260; i--) {
            for (let x = 0; x < this.image.width; x++) {
                let index = (x + i * this.image.width) * 4;
                let prev = (x + (i - 1) * this.image.width) * 4;

                this.image.pixels[index] = this.image.pixels[prev];
                this.image.pixels[index + 1] = this.image.pixels[prev + 1];
                this.image.pixels[index + 2] = this.image.pixels[prev + 2];
                this.image.pixels[index + 3] = this.image.pixels[prev + 3];
            }
        }


        this.image.updatePixels();


        image(this.image, 0, 0);

    }
}

module.exports = new mainBox();