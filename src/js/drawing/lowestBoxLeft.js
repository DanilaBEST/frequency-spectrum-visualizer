let Filter = require('../filter/index')
let Conf = require('../Config')

class secondBoxLeft {
    constructor() {
        this._ot = 768; //offset top
        this._ol = 1300 - 256;

        this._last = new Uint8Array(256);
    }

    draw(lastZoomed, lastFiltered, lastFilledColored) {


        let sum = 0;
        for (let i = 0; i < 1024; i++) {
            sum += lastFiltered[i];

            if ((i + 1) % 4 === 0) {
                let mean = sum / 4;
                sum = 0;
                let index = (i + 1) / 4 - 1;
                this._last[(i + 1) / 4 - 1] = this._last[index] > mean ? (mean + this._last[index] * 63) / 64 : (mean + this._last[index] * 7) / 8;
            }
        }

        let rgb = [0, 0, 0]
        for (let i = 0; i < 256; i++) {
            Filter.colors[Conf.colorMode](this._last[i], rgb);
            stroke(rgb);
            line(this._ol + i, this._ot + 8, this._ol + i, this._ot + 132)
        }
    }
}

module.exports = new secondBoxLeft();