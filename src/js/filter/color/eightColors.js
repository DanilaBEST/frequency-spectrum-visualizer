let values = [
    [0,0,0],
    [0,0,128],
    [128,0,128],
    [128,0,0],
    [200,80,0],
    [255,160,0],
    [235,235,0],
    [255,255,255],
];

module.exports = (val, rgb) => {

    let index = Math.trunc(val / 32);

    rgb[0] = values[index][0];
    rgb[1] = values[index][1];
    rgb[2] = values[index][2];
}