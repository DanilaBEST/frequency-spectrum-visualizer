module.exports = (val, rgb) => {
    rgb[0] = rgb[1] = rgb[2] = 0;

    let step = val % 32;

    if (val < 64) {
        rgb[2] = val;
    } else if (val < 96) {
        rgb[2] = 64 + step * 3;
    } else if (val < 128) {
        rgb[2] = 160;
        rgb[0] = step * 3;
    } else if (val < 160) {
        rgb[2] = 160;
        rgb[0] = 96 + step * 3;
    } else if (val < 192) {
        rgb[2] = 160 - step * 4;
        rgb[0] = 192 + step * 2;
    } else if (val < 224) {
        rgb[0] = 255;
        rgb[1] = step * 4;
    } else if (val < 240) {
        rgb[0] = 255;
        rgb[1] = step * 8 + 128;
    } else {
        rgb[0] = 255;
        rgb[1] = 255;
        rgb[2] = step * 8;
    }


    return rgb;
}