module.exports = (val, rgb) => {
    rgb[0] = rgb[1] = rgb[2] = 0;

    if (val < 128) {
        rgb[2] = 255 - 2 * val
    } else {
        rgb[0] = (val - 128) * 2
    }
}