let value_table = [];

for (let i = 0; i < 256; i++) {
    let part = 256 / Math.pow(256, 3);
    value_table.push(part * Math.pow(i, 3));
}

module.exports = (val_in, rgb) => {


    let val = value_table[Math.trunc(val_in)];


    rgb[0] = 0;
    rgb[1] = val;
    rgb[2] = 0;

    if (val > 192) {
        rgb[0] = (val - 210) * 4;
        rgb[2] = (val - 210) * 4;
    }
}