class Max extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Max";
        this.properties.name = "Max";
        this.properties.inputs = {
            threshold: {
                dpName: "Threshold",
                type: "number",
                min: 0,
                max: 255,
                value: 64,
            },
        }
    }

    calc(arr) {
        let threshold = parseFloat(this.properties.inputs.threshold.value);

        for (let i = 0; i < arr.length; i++) {
            arr[i] = arr[i] <= threshold ? arr[i] : 0;
        }
    }
}

module.exports = Max