class ZoomZ extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Zoom Upper Area";
        this.properties.name = "ZoomZ";
        this.properties.inputs = {
            threshold: {
                dpName: "Threshold",
                type: "number",
                min: 0,
                max: 255,
                value: 64,
            }
        }
    }

    calc(arr) {
        let threshold = parseFloat(this.properties.inputs.threshold.value);


        let factor = 256 / (256 - threshold);

        for (let i = 0; i < arr.length; i++) {
            arr[i] = arr[i] < threshold ? 0 : arr[i];
            arr[i] = arr[i] - threshold < 0 ? 0 : arr[i] - threshold;

            arr[i] *= factor;


            if (arr[i] > 255) {
                arr[i] = 255;
            }
        }
    }
}

module.exports = ZoomZ