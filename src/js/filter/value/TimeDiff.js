class TimeDiff extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = false;
        this.properties.dpName = "Different every frame";
        this.properties.name = "TimeDiff";
        this._last = new Uint8Array(1024);
        this._current = new Uint8Array(1024);
    }

    calc(arr) {

        for (let i = 0; i < arr.length; i++) {
            this._current[i] = arr[i];
            arr[i] = (this._current[i] - this._last[i]) / 2 + 128;
            this._last[i] = this._current[i];
        }
    }
}

module.exports = TimeDiff