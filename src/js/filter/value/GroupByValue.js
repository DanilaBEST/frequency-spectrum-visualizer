class Min extends require("./Root") {
    constructor() {
        super();

        this.properties.dpName = "Create Value Groups";
        this.properties.name = "GroupByValue";
        this.properties.inputs = {
            threshold: {
                dpName: "Threshold",
                type: "number",
                min: 0,
                max: 255,
                value: 64,
            }
        }
    }

    calc(arr) {
        let threshold = parseFloat(this.properties.inputs.threshold.value);


        let start_i = 0;
        let average = arr[0];
        let count = 2;

        for (let i = 1; i < arr.length; i++) {


            if (Math.abs(average - arr[i]) < threshold) {
                average = (average * count + arr[i]) / (count + 1);
                count++;
            } else {
                for (let j = start_i; j < i; j++) {
                    arr[j] = average;
                }

                start_i = i;
                count = 1;
                average = arr[i]
            }
        }

        for (let j = start_i; j < arr.length; j++) {
            arr[j] = average;
        }
    }
}

module.exports = Min