class BlockHighest extends require("./Root") {
    constructor() {
        super();

        this.properties.displayInGraph = true;
        this.properties.dpName = "Highest value of n blocks";
        this.properties.name = "BlockHighest";
        this.properties.inputs = {
            blocks: {
                dpName: "Blocks",
                type: "number",
                min: 0,
                max: 255,
                value: 64,
            }
        }
    }

    calc(arr) {
        let blocks = parseFloat(this.properties.inputs.blocks.value);

        let split = 1024 / blocks;

        /*if (split % 2 !== 0) {
            //return;
        }*/

        let highest = 0;
        let lastBlock = 0;

        for (let i = 0; i < 1024; i++) {
            if (arr[i] > highest) {
                highest = arr[i];
            }

            if (Math.trunc(i / split) > lastBlock) {
                for (let j = i - split; j <= i; j++) {
                    arr[j] = highest;
                }

                highest = 0;
                lastBlock++;
            }
        }
    }
}

module.exports = BlockHighest