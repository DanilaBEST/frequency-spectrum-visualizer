class SlowReturn extends require("./Root") {
    constructor() {
        super();

        this.properties.dpName = "Slow Return";
        this.properties.name = "SlowReturn";
        this.properties.inputs = {
            returnSpeed: {
                dpName: "Return Speed",
                type: "number",
                min: 1,
                max: 16,
                value: 2,
            }
        }

        this._last = new Int32Array(1024);
    }

    calc(arr) {
        let returnSpeed = parseFloat(this.properties.inputs.returnSpeed.value);

        for (let i = 0; i < arr.length; i++) {
            if (arr[i] > this._last[i]) {
                this._last[i] = arr[i];
            } else {
                this._last[i] -= returnSpeed;
                if (this._last[i] < 0) {
                    this._last[i] = 0;
                }

                arr[i] = this._last[i];
            }
        }
    }
}

module.exports = SlowReturn