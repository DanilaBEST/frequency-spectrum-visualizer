let _ = require('lodash');

class Config {
    constructor() {
        this.used_config = {
            hzLow: 0,
            hzHigh: 8000,
            resolution: 0,
            colorMode: "heat",
            graph: "bars32",
            zoomMode: 0,
            closeHoles: true,
            threeD: false,
            stop: false,
            _minDCB: -100,
            _maxDCB: -30,
            filters: []
        }

        for (let key in this.used_config) {
            this.bindGetSet(this, key);
        }

        this.load();
        //start
        this.stop = false;

    }

    reload() {
        window.location.reload()
    }

    get conf() {
        return this.used_config;
    }

    save() {
        let process = window.require('electron').remote.process;
        let storagePath = (process.env.APPDATA || (process.platform == 'darwin' ? process.env.HOME + '/Library/Preferences' : process.env.HOME + "/.local/share")) + '/fsv';
        let remote = window.require('electron').remote;
        let fs = remote.require('fs');

        fs.writeFileSync(storagePath + "/config.json", JSON.stringify(this.used_config));
    }

    load() {
        let process = window.require('electron').remote.process;
        let storagePath = (process.env.APPDATA || (process.platform == 'darwin' ? process.env.HOME + '/Library/Preferences' : process.env.HOME + "/.local/share")) + '/fsv';
        let remote = window.require('electron').remote;
        let fs = remote.require('fs');

        let json = fs.readFileSync(storagePath + "/config.json", 'utf8') || "{}";
        let config = JSON.parse(json);

        this.used_config = _.merge(this.used_config, config)

        //control hz low and high
        if (
            this.used_config.hzHigh < this.used_config.hzLow ||
            this.used_config.hzHigh == null ||
            this.used_config.hzLow == null
        ) {
            this.used_config.hzHigh = 24000;
            this.used_config.hzLow = 0;
        }
    }


    bindGetSet(o, property) {
        Object.defineProperty(o, property, {
            get: function () {
                return o.conf[property];
            },
            set: function (v) {
                o.conf[property] = v;
                o.save();
            }
        });
    }

    autoDCB() {

    }

    get minDCB() {
        return this.used_config._minDCB;
    }

    get maxDCB() {
        return this.used_config._maxDCB;
    }

    set minDCB(v) {

        v = parseInt(v);

        if (v < -120) {
            v = -120;
        }

        if (this.v + 20 > this.maxDCB) {
            v = this.maxDCB - 20;
        }


        analyser.minDecibels = v;
        this._minDCB = v;
    }

    set maxDCB(v) {

        v = parseInt(v);

        if (v > 0) {
            v = 0;
        }

        if (v < -80) {
            v = -80;
        }

        if (this.minDCB + 20 > v) {
            this.minDCB = v - 20;
        }

        analyser.maxDecibels = v;
        this._maxDCB = v;
    }
}

module.exports = new Config();
