class DebugInfo {

    /////////////////////constructor/////////////////////
    constructor() {
        this._frameRate = 40
    }

    /////////////////////Methods/////////////////////
    measure() {
        this._frameRate = ( this._frameRate * (800) + frameRate() * deltaTime) / (800+deltaTime);
    }

    /////////////////////GETTER/////////////////////
    get frameRate() {
        return Math.round(this._frameRate);
    }
    /////////////////////SETTER/////////////////////
}

module.exports = new DebugInfo();