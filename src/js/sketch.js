// p5 = require('./lib/p5');

let history = require("./History");
let conf = require("./Config");
let DebugInfo = require("./DebugInfo");


let drawParts = require("./drawing/drawParts");

draw = () => {
    if (analyser && !conf["stop"]) {


        history.add();


        //lowerBox
        drawParts();
        DebugInfo.measure();
    }
}


setTimeout(() => {
    try {
        let a =
            document.getElementById('box');

        document.getElementById('box').appendChild(document.getElementById('defaultCanvas0'));
        document.body.style.visibility = "visible";
    } catch (e) {
        console.error(e);
        window.location.reload()
    }
}, 2500);


