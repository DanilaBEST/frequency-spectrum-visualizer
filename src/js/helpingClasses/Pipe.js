let CyclicArray = require('./CyclicArray')

class Pipe {

    /////////////////////constructor/////////////////////
    constructor(size, defaultValue) {
        this._arr = new CyclicArray(size, defaultValue);
    }

    /////////////////////Methods/////////////////////

    add(data) {
        this._arr.add(data);
    }

    getByOffset(offset) {
        return this._arr.getByOffset(offset);
    }

    /////////////////////GETTER/////////////////////


    get mean() {
        let all = 0

        this._arr.forEach((value) => {
            all += value
        });

        return all / this._arr.length;
    }


    /////////////////////SETTER/////////////////////
}

module.exports = Pipe;