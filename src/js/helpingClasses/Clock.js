class Clock {
    constructor() {
        this._value = 0;
        this._start = 0;
    }

    start() {
        this._start = new Date().getTime();
    }

    stop() {

        if (!this._start) {
            return;
        }
        let val = new Date().getTime() - this._start;

        this._value = (val + this._value * 49) / 50;

        this._start = 0;
    }

    get mean() {
        return this._value;
    }

    log(nthFrame) {

        if (frameCount % (nthFrame || 8) === 0) {

            console.log("Time mean: " + Math.round(this._value));
        }
    }
}

module.exports = Clock;