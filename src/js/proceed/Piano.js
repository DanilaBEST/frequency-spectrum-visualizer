let conf = require("../Config").conf;
let _ = require("lodash");

class Piano {

    /////////////////////constructor/////////////////////
    constructor() {
        this._notes = [];
        this._minAmount = 128;
        this._minHitTime = 3;
        this._blockSize = 7;

        this._blockSizeHalf = (this._blockSize - 1) / 2;
        this._getNotesTable();
    }

    /////////////////////Methods/////////////////////
    detect(arr) {
        conf.hzLow = 0;
        conf.hzHigh = 2048;

        for (let i = 0; i < this._notes.length; i++) {
            let note = this._notes[i];

            if (arr[note.index] > this._minAmount && arr[note.index - 1] > this._minAmount && arr[note.index + 1] > this._minAmount) {
                note.hitFor++;
            } else {
                note.hitFor = 0;
            }
        }

        for (let i = 0; i < arr.length; i++) {
            arr[i] /= 3;
        }

        for (let i = 0; i < this._notes.length; i++) {
            let note = this._notes[i];

            if (note.hitFor >= this._minHitTime) {
                for (let j = 0; j <= this._blockSize; j++) {
                    arr[note.index + j - this._blockSizeHalf] = 255 - Math.abs(j - this._blockSizeHalf) * 10;
                }
            }
        }
    }

    _getNotesTable() {
        let groundNotes = [
            {
                letter: "c",
                value: 32.70320,
                prefix: 1,
            },
            {
                letter: "D",
                value: 36.70810,
                prefix: 1,
            },
            {
                letter: "e",
                value: 41.20344,
                prefix: 1,
            },
            {
                letter: "f",
                value: 43.65353,
                prefix: 1,
            },
            {
                letter: "g",
                value: 48.99943,
                prefix: 1,
            },
            {
                letter: "a",
                value: 55,
                prefix: 1,
            },
            {
                letter: "h",
                value: 61.73541,
                prefix: 1,
            },
        ];

        for (let i = 0; i < 7; i++) {
            for (let j = 0; j < 7; j++) {
                this._notes.push(_.cloneDeep(groundNotes[j]))

                groundNotes[j].prefix++;
                groundNotes[j].value *= 2;
            }
        }

        for (let i = 0; i < this._notes.length; i++) {
            this._notes[i].value = Math.round(this._notes[i].value);
            this._notes[i]["index"] = Math.round(this._notes[i].value / 2);
            this._notes[i]["hitFor"] = 0;
        }
    }

    /////////////////////GETTER/////////////////////
    /////////////////////SETTER/////////////////////
}

module.exports = new Piano();