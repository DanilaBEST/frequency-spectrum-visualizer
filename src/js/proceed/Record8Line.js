class Record8Line {

    /////////////////////constructor/////////////////////
    constructor() {
        this._loudes = undefined;
        this._last8 = [0, 0, 0, 0, 0, 0, 0, 0]
        this._new8 = [0, 0, 0, 0, 0, 0, 0, 0]
        this._lastOverlap = 0;
        this._index = 0;
        this._clear();
        this._deltaSum = 0;
        this._start = false;
        console.time();
    }

    /////////////////////Methods/////////////////////
    _clear() {
        this._loudes = [];
        this._lastOverlap = 0;

        for (let i = 0; i < 8; i++) {
            //16 ms = 1 entry
            //2048 entry = 16 sec
            this._loudes.push(new Uint8Array(2048))

            //clear last 8
            this._last8[i] = 0;
        }
    }

    record() {

    }

    addData(data) {

        for (let i = 0; i < data.length; i++) {
            if (data[i] > 10) {
                this._start = true;
            }
        }

        if (this._index > 512 || !this._start) {
            return;
        }

        for (let i = 0; i < 8; i++) {
            let start = i * 128;
            let end = i * 128 + 128;

            let high = 0;
            let high_index = 0;

            for (let j = start; j < end; j++) {
                if (data[j] > high) {
                    high_index = j - start;
                    high = data[j];
                }
            }

            this._new8[i] = high_index;
        }

        let delta = deltaTime;
        this._deltaSum += delta;

        if (this._lastOverlap) {
            delta -= this._lastOverlap;

            for (let i = 0; i < 8; i++) {
                try {

                    this._loudes[i][this._index] = (this._last8[i] * this._lastOverlap + this._new8[i] * (16 - this._lastOverlap)) / 16
                } catch (e) {
                    debugger
                }
            }

            this._lastOverlap = 0;
            this._index++;
        }

        while (delta >= 16) {
            for (let i = 0; i < 8; i++) {
                this._loudes[i][this._index] = this._new8[i];
            }

            delta -= 16;
            this._index++;
        }

        this._lastOverlap = delta;
        for (let i = 0; i < 8; i++) {
            this._last8[i] = this._new8[i];
        }

        //this.draw()
    }

    draw() {
        if (this._index < 512) {
            return;
        }

        let History = require("../History");
        background(255);
        let col = [0, 0, 0];

        for (let i = 0; i < 2048; i++) {
            for (let j = 0; j < 8; j++) {
                let cell = this._loudes[j][i];
                History.colFilter(cell * 2, col);
                stroke(col)
                line(i, j * 8, i, j * 8 + 8)
            }
        }
    }

    /////////////////////GETTER/////////////////////


    /////////////////////SETTER/////////////////////
}

module.exports = new Record8Line();