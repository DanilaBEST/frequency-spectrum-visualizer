let Pipe = require("../helpingClasses/Pipe");

class debugInfo {

    /////////////////////constructor/////////////////////
    constructor() {
        this._frameRate = new Pipe(64, 50);
    }

    /////////////////////Methods/////////////////////
    /////////////////////GETTER/////////////////////
    get frameRate() {
        return this._frameRate.mean();
    }
    /////////////////////SETTER/////////////////////
}

module.exports = new debugInfo();